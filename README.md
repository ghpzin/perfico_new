# Perfico_new #

Главная страница сайта perfi.co

Верстка выполнена строго по полученному [PSD](https://drive.google.com/open?id=0BxT24xrmH7OMc3YtVWlhVWNyNTg) макету, поэтому весь контент в блоках одинаковый (карусель, Helpful Articles и подменю Products в шапке).

Рабочий код находится в ветке [develop](https://bitbucket.org/ghpzin/perfico_new/src/7e5da36c1192c4e99e2cea0cef9b928dca75661c?at=develop).

[Ссылка на gif с демонстрацией](https://drive.google.com/open?id=0BxT24xrmH7OMeTM2VGZwY3psa2M)